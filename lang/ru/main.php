<?php

return [

    //Categories
    'MainNews'      => 'Главные Новости',
    'Interview'     => 'Интервью',
    'Majornews'     => 'Главные Новости',
    'Culture'       => 'Культура',
    'Politics'      => 'Политика',
    'Sport'         => 'Спорт',
    'Society'       => 'Общество',
    'World'         => 'Мир',
    'Economic'      => 'Экономика',
    'Occasion'      => 'События',
    'Weather'       => 'Погода',
    'Literature'    => 'Литература',
    'Show Business' => 'Шоу Бизнес',
    'Crime'         => 'Криминал',
    'Ecology'       => 'Экология',
    'Allnews'       => 'Все новости',
    'Recommended'   => 'Рекомендуемые',

    //Pages
    'Contacts'      => 'Контакты',
    'Rules'         => 'правила ползование сайтом',

    //Days
    'Monday'        => 'Понедельник',
    'Tuesday'       => 'Вторник',
    'Wednesday'     => 'Cреда',
    'Thursday'      => 'Четверг',
    'Friday'        => 'Пятница',
    'Saturday'      => 'Суббота',
    'Sunday'        => 'Воскресенье',

    //Month
    'January'       => 'январь',
    'February'      => 'февраль',
    'March'         => 'март',
    'April'         => 'апрель',
    'May'           => 'май',
    'June'          => 'июнь',
    'July'          => 'июль',
    'August'        => 'август',
    'September'     => 'сентябрь',
    'October'       => 'октябрь',
    'November'      => 'ноябрь',
    'December'      => 'декабрь',
];


