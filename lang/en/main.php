<?php

return [
    //Categories
    'MainNews'      => 'Main-News',
    'Interview'     => 'Interview',
    'Majornews'     => 'Majornews',
    'Culture'       => 'Culture',
    'Politics'      => 'Politics',
    'Sport'         => 'Sport',
    'Society'       => 'Society',
    'World'         => 'World',
    'Economic'      => 'Economic',
    'Show Business' => 'Show Business',
    'Occasion'      => 'Occasion',
    'Weather'       => 'Weather',
    'Literature'    => 'Literature',
    'Crime'         => 'Crime',
    'Court'         => 'Court',
    'Ecology'       => 'Ecology',
    'Allnews'       => 'Allnews',
    'Recommended'   => 'Recommended',

    //Pages
    'Rules'         => 'Rules',
    'Contacts'      => 'Contacts',

    //Days
    'Monday'        => 'Monday',
    'Tuesday'       => 'Tuesday',
    'Wednesday'     => 'Wednesday',
    'Thursday'      => 'Thursday',
    'Friday'        => 'Friday',
    'Saturday'      => 'Saturday',
    'Sunday'        => 'Sunday',

    //Month
    'January'       => 'January',
    'February'      => 'February',
    'March'         => 'March',
    'April'         => 'April',
    'May'           => 'May',
    'June'          => 'June',
    'July'          => 'July',
    'August'        => 'August',
    'September'     => 'September',
    'October'       => 'October',
    'November'      => 'November',
    'December'      => 'December',
];
