<?php

return [
    //Categories
    'MainNews'      => 'Басты жаңалықтар',
    'Interview'     => 'Сұхбат',
    'Majornews'     => 'Басты Жаңалықтар',
    'Culture'       => 'Мәдениет',
    'Politics'      => 'Саясат',
    'Sport'         => 'Спорт',
    'Society'       => 'Қоғам',
    'World'         => 'Әлем',
    'Economic'      => 'Экономика',
    'Show Business' => 'Шоу Бизнес',
    'Occasion'      => 'Оқиға',
    'Weather'       => 'Ауа райы',
    'Literature'    => 'Әдебиет',
    'Crime'         => 'Қылмыс',
    'Court'         => 'Сот',
    'Ecology'       => 'Экология',
    'Allnews'       => 'Жаңалықтар',
    'Recommended'   => 'Ұсынылатын',

    //Pages
    'Rules'         => 'сайтты пайдалану ережелері',
    'Contacts'      => 'Байланыс',

    //Days
    'Monday'        => 'Дүйсенбі',
    'Tuesday'       => 'Cейсенбi',
    'Wednesday'     => 'Cәрсенбi',
    'Thursday'      => 'Бейсенбi',
    'Friday'        => 'Жұма',
    'Saturday'      => 'Сенбі',
    'Sunday'        => 'Жексенбі',

    //Month
    'January'       => 'қаңтар',
    'February'      => 'ақпан',
    'March'         => 'наурыз',
    'April'         => 'сәуір',
    'May'           => 'мамыр',
    'June'          => 'маусым',
    'July'          => 'шілде',
    'August'        => 'тамыз',
    'September'     => 'қыркүйек',
    'October'       => 'қазан',
    'November'      => 'қараша',
    'December'      => 'желтоқсан',
];
