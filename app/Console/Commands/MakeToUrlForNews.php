<?php

namespace App\Console\Commands;

use App\Models\News;
use Illuminate\Console\Command;

class MakeToUrlForNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:url_news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'URL для новостей сгенерированы';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $items = News::select('id', 'title', 'url')->get();
        foreach ($items as $item) {
            $item->url = str_slug($item->title).'-'.$item->id;
            $item->save();
        }

        return 'Url для новостей успешно сгенерированы';
    }
}
