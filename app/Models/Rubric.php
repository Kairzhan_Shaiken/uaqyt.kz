<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rubric extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table='rubric';

    protected $id;
    protected $name;
    protected $status;

    public function get_latest_three_news()
    {
        return $this->belongsToMany(News::class,'news_rubrics_connection','rubric_id','news_id')->latest()->take(3)->where('video', null);
    }

    public function get_news2()
    {
        return $this->belongsToMany(News::class,'news_rubrics_connection','rubric_id','news_id')->latest()->take(4);
    }
}
