<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table='advertisment';

    protected $id;
    protected $image;
    protected $url;
    protected $title;
    protected $eventdescription;
    protected $participation;
}
