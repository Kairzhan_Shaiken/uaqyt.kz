<?php


namespace App\Http\Controllers\Admin\Statistics;


use App\Http\Controllers\Controller;
use App\Models\News;

class StatisticsController extends Controller
{
    public function index()
    {
        $allnews=News::orderby('views', 'DESC')->paginate(8);
        return view('admin.statistics.list',compact('allnews'));
    }




}
