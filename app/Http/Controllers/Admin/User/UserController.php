<?php


namespace App\Http\Controllers\Admin\User;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $allusers=User::all();
        return view('admin.users.list',compact('allusers'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function show($user_id)
    {
         $certain_user=User::find($user_id);
         return view('admin.users.edit',compact('certain_user'));
    }

    public function store()
    {
        $data=request()->validate([
            'name'=> '',
            'email'=> '',
            'password'=> ''
        ]);

        $password=$data ['password'];
        $hashedpassword=bcrypt($password);
        unset($data ['password']);
        $data['password']=$hashedpassword;
        User::create($data);
        return redirect()->route('users');
    }

    public function update($user_id)
    {
        $certain_user=User::find($user_id);

        $data=request()->validate([
            'name'=> '',
            'email'=> '',
            'password'=> ''
        ]);

        $password=$data ['password'];
        $hashedpassword=bcrypt($password);
        unset($data ['password']);
        $data['password']=$hashedpassword;


        $certain_user->update($data);
        return redirect()->route('users');
    }

    public function destroy($user_id)
    {
        $certain_user=User::find($user_id);
        $$certain_user->delete();
        return redirect()->route('users');
    }
}
