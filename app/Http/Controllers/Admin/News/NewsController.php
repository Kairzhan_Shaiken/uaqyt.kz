<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Categories_News;
use App\Models\News;
use App\Models\News_Rubrics;
use App\Models\Rubric;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

include_once(app_path('Services/simple_html_dom.php'));

class NewsController  extends Controller
{

    public function index()
    {
        $allnews = News::orderby ('created_at', 'DESC')->paginate(8);
        return view ('admin.news.list',compact('allnews'));
    }

    public function create()
    {
        $categories = Categories::all();
        $rubrics = Rubric::all();
        $users = User::all();
        Carbon::setLocale('ru_RU');
        $today = Carbon::parse(date("D j M"));

        return view('admin.news.create',compact('categories','rubrics','users','today'));
    }

    public function show($news_id)
    {
        $item = News::find($news_id);
        $categories = Categories::all();
        $rubrics = Rubric::all();
        $users = User::all();
        return view('admin.news.edit',compact('item','categories','rubrics','users'));
    }

    public function store()
    {
        $data = request()->validate([
            'image' => '',
            'title' => 'required',
            'title_rus' => 'required',
            'title_eng' => 'required',
            'author_id' => 'required',
            'content' => '',
            'content_rus' => '',
            'content_eng' => '',
            'seotitles' =>'',
            'seodescriptions' => '',
            'seokeywords' => '',
            'сategories' => '',
            'rubrics' => '',
            'date_of_creation' => 'required',
            'visibility' => 'required',
            'video' => '',
            'image_description' => ''
        ]);

        $categories = $data['сategories'];
        $rubrics = $data['rubrics'];
        unset($data['сategories']);
        unset($data['rubrics']);

        if(isset($data['image'])) {
            $image_path = Storage::disk('public')->put('/images',$data['image']);//put loaded image to storage
            $data['image'] = $image_path;//set url for image
        }

        $post = News::create($data);

        // url
        $post->url = str_slug($post->title).'-'.$post->id;

        // seo
        $post->SEOtitles = $post->title;
        $desc = str_get_html($post->content);
        if($desc) {
            $post->SEOdescriptions = strip_tags($desc->find('p',)[0]->outertext());
        }

        //
        $post->save();

        foreach ($categories as $category){
            Categories_News::firstOrCreate([
                'news_id'=>$post->id,
                'category_id'=>$category
            ]);
        }

        foreach ($rubrics as $rubric){
            News_Rubrics::firstOrCreate([
                'news_id'=>$post->id,
                'rubric_id'=>$rubric
            ]);
        }

        return redirect()->route('allnews');
    }

    public function update(Request $request, $news_id)
    {
        $data = $request->validate([
            'image'=>'',
            'title'=>'required',
            'title_rus' => 'required',
            'title_eng' => 'required',
            'author_id'=>'required',
            'content'=>'required|min:10',
            'content_rus' => '',
            'content_eng' => '',
            'seotitles'=>'',
            'seodescriptions'=>'',
            'seokeywords'=>'',
            'сategories'=>'required',
            'rubrics'=>'required',
            'date_of_creation'=>'required',
            'visibility'=>'required',
            'video'=>'',
            'image_description'=>''
        ]);


        $categories = $data ['сategories'];
         $rubrics = $data ['rubrics'];

        unset($data['сategories']);
        unset($data['rubrics']);

        $certain_news = News::find($news_id);

        if( isset($data['image'])){
        Storage::disk('public')->delete($certain_news->image);
        $image_path=Storage::disk('public')->put('/images',$data['image']);
        $data['image']=$image_path;//set url for image
        }

        else

        $certain_news->get_categories()->sync($categories);
        $certain_news->get_rubrics()->sync($rubrics);
        $certain_news->update($data);
        return redirect()->route('allnews');
    }

    public function destroy($news_id)
    {
        $certain_news=News::find($news_id);

        Categories_News::where(['news_id'=>$news_id])
                       ->delete();

        News_Rubrics::where(['news_id'=>$news_id])
                        ->delete();

        $certain_news->delete();
        return redirect()->route('allnews');
    }
}
