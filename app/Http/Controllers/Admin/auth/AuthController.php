<?php


namespace App\Http\Controllers\Admin\auth;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;


class AuthController extends Controller
{

    /**
     * @throws ValidationException
     */
    public function index(){
        return view('admin.auth.login');
    }



    public function auth(Request $request)
    {
        if ($request->get('email') !== null){
            $validated = $request->validate([
                'email'=>'required',
                'password'=>'required'
            ]);
            if (!Auth::attempt($validated)){
                throw ValidationException::withMessages([
                    'email' => 'error email or password'
                ]);
            }
        }
        if (Auth::check()) {
            $request->session()->regenerate();
            return to_route('allnews');
        }

    }

//    public function logout()
//    {
//        auth()->logout();
//        return to_route('admin.login');
//    }
}
