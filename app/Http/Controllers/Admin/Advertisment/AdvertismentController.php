<?php


namespace App\Http\Controllers\Admin\Advertisment;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Categories;
use App\Models\Categories_News;
use App\Models\News;
use App\Models\News_Rubrics;
use App\Models\Rubric;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class AdvertismentController extends Controller
{
    public function index()
    {
        $alladvertisment = Banner::all();
        return view('admin.advertisment.list', compact('alladvertisment'));
    }

    public function create()
    {
        return view('admin.advertisment.create');
    }

    public function store()
    {
        $data = request()->validate([
            'image' => 'required',
//            'title' => 'required',
            'eventdescription' => 'required',
        ]);
        $image_path = Storage::disk('public')->put('/images', $data['image']);//put loaded image to storage
        $data['image'] = $image_path;//set url for image
        $last_created_banner = Banner::create($data);
        return redirect()->route('advertisment');
    }

    public function show($banner_id)
    {
        $certain_advertisment = Banner::find($banner_id);
        return view('admin.advertisment.edit', compact('certain_advertisment'));
    }

    public function update(Request $request, $banner_id)
    {
        $data = request()->validate([
            'image' => 'required',
//            'title' => 'required',
            'eventdescription' => 'required',
        ]);

        $certain_advertisment= Banner::find($banner_id);
        Storage::disk('public')->delete($certain_advertisment->image);
        $image_path=Storage::disk('public')->put('/images',$data['image']);
        $data['image']=$image_path;//set url for image
        $certain_advertisment->update($data);
        return redirect()->route('advertisment');

    }


    public function destroy($banner_id)
    {
        $certain_advertisment= Banner::find($banner_id);
        $certain_advertisment->delete();
        return redirect()->route('advertisment');
    }
}
