<?php


namespace App\Http\Controllers\Admin\Image;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    public function fileUploads(Request $request)
    {
        $data = $request->file();
        $image_path = Storage::disk('public')->put('/images',$data['upload']);//put loaded image to storage
        $funcNum = $request->input('CKEditorFuncNum');

        $image_path=asset('storage/' . $image_path);
        return response(
            "<script>
            window.parent.CKEDITOR.tools.callFunction({$funcNum},'{$image_path}', 'Изображение успешно загружено');
        </script>"
        );


    }
}

