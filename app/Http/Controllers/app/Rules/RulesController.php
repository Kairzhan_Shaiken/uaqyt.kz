<?php


namespace App\Http\Controllers\app\Rules;
use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\News;
use App\Models\Pages;
use Carbon\Carbon;

class RulesController extends Controller
{

    public function index()
    {
        $rules=Pages::find(2);
        $allcategories=Categories::all();
        Carbon::setLocale('ru_RU');
        $today = Carbon::parse(date("D j M"));
        return view('app.pages.rules',compact('rules','allcategories','today'));
    }

}
