<?php


namespace App\Http\Controllers\app\News;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Categories;
use App\Models\News;
use App\Models\Rubric;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;



class NewsController extends Controller
{

    public function index()
    {
        $allnews = News::orderby('created_at', 'DESC')->where('visibility',1)->limit(36)->get();

        $mainnews_rubric = Rubric::find(1);
        $mainnews=$mainnews_rubric->get_latest_three_news;//get news for main news area

        $slidernews = $allnews->sortDesc()->take(5)->where('video',null);//get news for slider news area

        $main = $allnews->where('video',null)->chunk(3);//get news for slider news area


        $categories=Categories::all();
        $banner=Banner::find(1);
        $extranews = News::all()->shuffle();
        Carbon::setLocale('ru_RU');
        $today = Carbon::parse(date("D j M"));
        $currentlocale=App::getLocale();

        return view('app.pages.main',compact('slidernews','mainnews','allnews', 'main','categories','today','extranews','banner','currentlocale'));

    }

    public function show($url)
    {

        $item = News::where('url',$url)->first();
        $item->views = $item->views + 1;
        $item->save();

        $recommended = Rubric::find(2)->get_news2;
        $categories = Categories::all();

        Carbon::setLocale('ru_RU');
        $today = Carbon::parse(date("D j M"));
        $currentlocale=App::getLocale();

        return view('app.pages.certainnews',compact('item','categories','recommended','today','currentlocale'));
    }

    public function search(){

        $data=request()->validate([
            'keyword'=>'required'
        ]);
        $keyword=$data['keyword'];
        $finded=News::where('content','LIKE', "%$keyword%")->get();
        $categories=Categories::all();
        Carbon::setLocale('ru_RU');
        $today = Carbon::parse(date("D j M"));
        $currentlocale=App::getLocale();
        return view('app.pages.findedlist',compact('finded','today','categories','currentlocale'));
    }

    public function locale($locale)
    { App::setLocale($locale);
        $currentlocale=App::getLocale();
        session(['locale'=>$locale]);
        return back();
    }

}
