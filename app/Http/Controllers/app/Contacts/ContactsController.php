<?php


namespace App\Http\Controllers\app\Contacts;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Pages;
use Carbon\Carbon;

class ContactsController extends Controller
{
    public function index()
    {
        $contacts=Pages::find(1);
        $allcategories=Categories::all();
        Carbon::setLocale('ru_RU');
        $today = Carbon::parse(date("D j M"));
        return view('app.pages.contacts' ,compact('contacts','allcategories','today'));
    }
}
