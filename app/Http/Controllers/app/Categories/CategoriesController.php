<?php


namespace App\Http\Controllers\app\Categories;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Pages;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class CategoriesController extends Controller
{
    public function index($categories_id)
    {

        $category=Categories::find($categories_id);
        $allnewsofeachcategory=$category->get_news()->paginate(7);
        $categories=Categories::all();
        Carbon::setLocale('ru_RU');
        $today = Carbon::parse(date("D j M"));
        $currentlocale=App::getLocale();
        return view('app.pages.news',compact('categories','allnewsofeachcategory','today','category','currentlocale'));
    }
}
