<!DOCTYPE html>
<link rel="shortcut icon" type="image/x-icon" href="./img/yaqyt.png" />

@extends('app.layout.layouts')

@section('meta')

    <!-- Open Graph Generated: a.pr-cy.ru -->
    <meta property="og:type" content="website">
    <meta charset="utf-8">
    <meta property="og:title" content="{{$item->title}}">
    <meta property="og:description" content="{{$item->SEOtitle}}">
    <meta property="og:url" content="{{ route('each_news', $item->title) }}">
    <meta property="og:image" content="{{urldecode(url('storage',$item->image))}}">


@endsection

@section('content')
    <section class="news">
        <div class="container">
            <ul class="news-path">
                <li>
                    <a href="{{route('mainpage')}}">{{__('main.MainNews')}} / </a>
                </li>
                <li>
                    @foreach($item->get_categories as $category)
                        <a href="{{route('categories_all',$category->id)}}" class="info-text-category">
                            @if($category->id==1){{__('main.Culture')}}@endif
                            @if($category->id==2){{__('main.Politics')}}@endif
                            @if($category->id==3){{__('main.Sport')}}@endif
                            @if($category->id==4){{__('main.Society')}}@endif
                            @if($category->id==5){{__('main.World')}}@endif
                            @if($category->id==6){{__('main.Economic')}}@endif
                            @if($category->id==7){{__('main.Show Business')}}@endif
                            @if($category->id==8){{__('main.Occasion')}}@endif
                            @if($category->id==9){{__('main.Weather')}}@endif
                            @if($category->id==10){{__('main.Crime')}}@endif
                            @if($category->id==11){{__('main.Literature')}}@endif
                        </a>
                    @endforeach
                </li>
                <li>
                    <a href="{{route('each_news',$item->title)}}" class="active">
                        @if($currentlocale=='kz'){{$item->title}}@endif
                        @if($currentlocale=='ru'){{$item->title_rus}}@endif
                        @if($currentlocale=='en'){{$item->title_eng}}@endif
                    </a>
                </li>
            </ul>
            <div class="news-main-title">
                <h2>
                    @if($currentlocale=='kz'){{$item->title}}@endif
                    @if($currentlocale=='ru'){{$item->title_rus}}@endif
                    @if($currentlocale=='en'){{$item->title_eng}}@endif
                </h2>
                <div class="basic-info">
                    <span>{{$item->author}}</span>
                    <span><img src="/img/icon/time.svg" alt="">{{$item->date_of_creation}}</span>
                    <span><img src="/img/icon/eye.svg" alt="">{{$item->views}}</span>
                </div>
                <div class="news-photo">
                    @if(isset($item->image))
                        <img src="{{urldecode(url('storage',$item->image))}}" alt="">
                        <span class="news-photo-info">Фото {{$item->image_description}}</span>
                    @endif
                    @if(isset($item->video))
                        <iframe id="inlineFrameExample"
                                title="Inline Frame Example"
                                width="657"
                                height="350"
                                src="{!! $item->video!!}">
                        </iframe>
                    @endif
                </div>
                <div class="news-photo-wrapper">
                    <div class="col-md-8 col-md-push-1 news-text">
                        @if($currentlocale=='kz'){!!$item->content!!}@endif
                        @if($currentlocale=='ru'){!!$item->content_rus!!}@endif
                        @if($currentlocale=='en'){!!$item->content_eng!!}@endif
                        <div class="link-social">
                            <a href="https://twitter.com/intent/tweet?url{{route('each_news',$item->title)}}">
                                <img src="/img/icon/tt.png" alt="">
                            </a>
                            <a href="http://www.facebook.com/sharer/sharer.php?u={{ route('each_news',$item->title) }}">
                                <img src="/img/icon/fb.png" alt="">
                            </a>
                            <a href="https://telegram.me/share/url?url={{ route('each_news', $item->title) }}">
                                <img src="/img/icon/tg.png" alt="">
                            </a>
                            <a href="https://vk.com/share.php?url={{ route('each_news', $item->title) }}">
                                <img src="/img/icon/vk.png" alt="">
                            </a>
                            <a href="https://api.whatsapp.com/send?text={{route('each_news', $item->title)}}">
                                <img src="/img/icon/wa.png" alt="">
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="title extra-title">
                <h4>{{__('main.Recommended')}}</h4>
            </div>
            <div class="inner-extra-news">
                @foreach($recommended as $rec_news)
                    <div class="extra-news-item">
                        @if(isset($rec_news->image))
                            <a href="{{route('each_news',$rec_news->url)}}"><img src="{{urldecode(url('storage',$rec_news->image))}}" alt=""></a>
                        @endif
                        @if(isset($rec_news->video))
                            <iframe id="inlineFrameExample"
                                    title="Inline Frame Example"
                                    width="250"
                                    height="160"
                                    src="{!! $rec_news->video!!}">
                            </iframe>
                        @endif

                        <div  class="extra-news-info">
                            @foreach($rec_news->get_categories as $category)
                                <a href="{{route('categories_all',$category->id)}}" class="info-text-category">
                                    @if($category->id==1){{__('main.Culture')}}@endif
                                    @if($category->id==2){{__('main.Politics')}}@endif
                                    @if($category->id==3){{__('main.Sport')}}@endif
                                    @if($category->id==4){{__('main.Society')}}@endif
                                    @if($category->id==5){{__('main.World')}}@endif
                                    @if($category->id==6){{__('main.Economic')}}@endif
                                    @if($category->id==7){{__('main.Show Business')}}@endif
                                    @if($category->id==8){{__('main.Occasion')}}@endif
                                    @if($category->id==9){{__('main.Weather')}}@endif
                                    @if($category->id==10){{__('main.Crime')}}@endif
                                    @if($category->id==11){{__('main.Literature')}}@endif
                                </a>
                            @endforeach
                            <h3>
                                <a href="{{route('each_news',$rec_news->url)}}">
                                    @if($currentlocale=='kz'){!!$rec_news->title!!}@endif
                                    @if($currentlocale=='ru'){!!$rec_news->title_rus!!}@endif
                                    @if($currentlocale=='en'){!!$rec_news->title_eng!!}@endif
                                </a>
                            </h3>
                            <span><img src="/img/icon/time.svg" alt="">{{$rec_news->date_of_creation}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
