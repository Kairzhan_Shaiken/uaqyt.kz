
@extends('app.layout.layouts')
@section('content')
    <section class="news">
        <div class="container">
            @foreach($finded as $news)
            <div class="news-main-title">
                <h2>
                    @if($currentlocale=='kz'){{$news->title}}@endif
                    @if($currentlocale=='ru'){{$news->title_rus}}@endif
                    @if($currentlocale=='en'){{$news->title_eng}}@endif
                </h2>
                <div class="basic-info">
                    <span>{{$news->author}}</span>
                    <span><img src="/img/icon/time.svg" alt="">{{$news->date_of_creation}}</span>
                    <span><img src="/img/icon/eye.svg" alt="">{{$news->views}}</span>
                </div>
                <div class="news-photo">
                    @if(isset($news->image))
                    <img src="{{urldecode(url('storage',$news->image))}}" alt="">
                    @endif

                    @if(isset($news->video))
                            <iframe id="inlineFrameExample"
                                    title="Inline Frame Example"
                                    width="657"
                                    height="350"
                                    src="{!! $news->video!!}">
                            </iframe>
                    @endif
                </div>
                <div class="news-photo-wrapper">
                    <div class="col-md-8 col-md-push-1 news-text">
                        @if($currentlocale=='kz'){!!$news->content!!}@endif
                        @if($currentlocale=='ru'){!!$news->content_rus!!}@endif
                        @if($currentlocale=='en'){!!$news->content_eng!!}@endif
                        <div class="link-social">
                            <a href="">
                                <img src="/img/icon/tt.png" alt="">
                            </a>
                            <a href="">
                                <img src="/img/icon/fb.png" alt="">
                            </a>
                            <a href="">
                                <img src="/img/icon/tg.png" alt="">
                            </a>
                            <a href="">
                                <img src="/img/icon/vk.png" alt="">
                            </a>
                            <a href="">
                                <img src="/img/icon/wa.png" alt="">
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </section>
@endsection


