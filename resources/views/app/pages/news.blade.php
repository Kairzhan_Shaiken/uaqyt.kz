<link rel="shortcut icon" type="image/x-icon" href="./img/yaqyt.png" />

@extends('app.layout.layouts')
@section('content')
    <section class="category">
        <div class="container">
            <div class="category-title">
                <h4>
                    @if($category->id==1){{__('main.Culture')}}@endif
                    @if($category->id==2){{__('main.Politics')}}@endif
                    @if($category->id==3){{__('main.Sport')}}@endif
                    @if($category->id==4){{__('main.Society')}}@endif
                    @if($category->id==5){{__('main.World')}}@endif
                    @if($category->id==6){{__('main.Economic')}}@endif
                    @if($category->id==7){{__('main.Show Business')}}@endif
                    @if($category->id==8){{__('main.Occasion')}}@endif
                    @if($category->id==9){{__('main.Weather')}}@endif
                    @if($category->id==10){{__('main.Crime')}}@endif
                    @if($category->id==11){{__('main.Literature')}}@endif
                </h4>
            </div>
            <div class="category-info">
                @foreach($allnewsofeachcategory as $news)
                <div class="category-news-item">
                        <div class="category-news-img">
                            @if(isset($news->image))
                            <a href="{{route('each_news',urlencode(str_slug($news->url)))}}"><img src="{{urldecode(url('storage',$news->image))}}" alt=""></a>
                            @endif

                            @if(isset($news->video))
                                    <iframe id="inlineFrameExample"
                                            title="Inline Frame Example"
                                            width="270"
                                            height="160"
                                            src="{!! $news->video !!}">
                                    </iframe>
                            @endif
                        </div>
                    <div class="category-news-info">
                        @foreach($news->get_categories as $category)
                            <a href="{{route('categories_all',$category->id)}}" class="info-text-category">
                                @if($category->id==1){{__('main.Culture')}}@endif
                                @if($category->id==2){{__('main.Politics')}}@endif
                                @if($category->id==3){{__('main.Sport')}}@endif
                                @if($category->id==4){{__('main.Society')}}@endif
                                @if($category->id==5){{__('main.World')}}@endif
                                @if($category->id==6){{__('main.Economic')}}@endif
                                @if($category->id==7){{__('main.Show Business')}}@endif
                            </a>
                        @endforeach
                            <h3>
                                <a href="{{route('each_news',urlencode(str_slug($news->url)))}}">
                                    @if($currentlocale=='kz'){{$news->title}}@endif
                                    @if($currentlocale=='ru'){{$news->title_rus}}@endif
                                    @if($currentlocale=='en'){{$news->title_eng}}@endif
                                </a>
                            </h3>
                        <div class="autor">
                        <span><img src="/img/icon/time.svg" alt="">{{$news->date_of_creation}}</span>
                        <span>{{$news->author}}</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{$allnewsofeachcategory->links()}}
        </div>

    </section>
@endsection


