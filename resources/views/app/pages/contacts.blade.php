<link rel="shortcut icon" type="image/x-icon" href="./img/yaqyt.png" />
@extends('app.layout.layouts')
@section('content')
    <section class="contacts">
        <div class="container">
            <ul class="news-path">
                <li>
                    <a href="">    {{__('main.MainNews')}} </a>
                </li>
                <li>
                    <a href="{{route('contacts')}}" class="active">{{__('main.Contacts')}}</a>
                </li>
            </ul>
            <div class="news-main-title">
                <h2>Контакты</h2>
            </div>
            <div class="contacts">
                <div class="contacts-info-item-text">
                    <p>{!! $contacts->content !!} </p>
                </div>
            </div>
            <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2905.438058838625!2d76.92706142167093!3d43.26319772863002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836ea5c1ce6533%3A0x9cee2843b40aadbb!2z0YPQuy4g0JzQsNC60LDRgtCw0LXQstCwIDEyNywg0JDQu9C80LDRgtGLIDA1MDAwMA!5e0!3m2!1sru!2skz!4v1678559283786!5m2!1sru!2skz" width="100%" height="450" style="border:0; border-radius: 6px" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </section>
@endsection
