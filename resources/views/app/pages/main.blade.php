<link rel="shortcut icon" type="image/x-icon" href="./img/yaqyt.png" />

@extends('app.layout.layouts')
@section('content')
    <section class="main-content">
        <div class="container">
            <div class="row main-wrapper">
                <div class="col-md-8 main-wrapper-one">
                    <div class="first-row row">
                        <div class="col-md-12 one-wrapper">
                            <div class="main__swiper">
                                <div class="swiper mainSwiper">
                                    <div class="swiper-wrapper">
                                        @foreach($slidernews as $sl_news)
                                        <div class="swiper-slide">
                                            <div class="carousel">
                                                <div class="inner-carousel">
                                                    <a href="{{route('each_news',urlencode(str_slug($sl_news->url)))}}"><img src="{{urldecode(url('storage',$sl_news->image))}}" alt=""></a>
                                                    <div  class="news-info">
                                                        <p> @foreach($sl_news->get_categories as $category)
                                                               <a href="{{route('categories_all',$category->id)}}">
                                                                   @if($category->id==1){{__('main.Culture')}}@endif
                                                                   @if($category->id==2){{__('main.Politics')}}@endif
                                                                   @if($category->id==3){{__('main.Sport')}}@endif
                                                                   @if($category->id==4){{__('main.Society')}}@endif
                                                                   @if($category->id==5){{__('main.World')}}@endif
                                                                   @if($category->id==6){{__('main.Economic')}}@endif
                                                                   @if($category->id==7){{__('main.Show Business')}}@endif
                                                                   @if($category->id==8){{__('main.Occasion')}}@endif
                                                                   @if($category->id==9){{__('main.Weather')}}@endif
                                                                   @if($category->id==10){{__('main.Crime')}}@endif
                                                                   @if($category->id==11){{__('main.Literature')}}@endif
                                                                   @if($category->id==13){{__('main.Court')}}@endif
                                                                   @if($category->id==15){{__('main.Ecology')}}@endif
                                                               </a>
                                                            @endforeach
                                                        </p>
                                                        <h4><a href="{{route('each_news', urlencode(str_slug($sl_news->url)))}}">
                                                                @if($currentlocale=='kz'){!!$sl_news->title!!}@endif
                                                                @if($currentlocale=='ru'){!!$sl_news->title_rus!!}@endif
                                                                @if($currentlocale=='en'){!!$sl_news->title_eng!!}@endif
                                                            </a>
                                                        </h4>
                                                        <span>{{$sl_news->date_of_creation}},  {{$sl_news->get_author->name}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="mainSwiper-control">
                                        <div class="swiper-button-next-1">
                                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect width="30" height="30" rx="6" fill="white"/>
                                                <path d="M18 9L12 15L18 21" stroke="#555555" stroke-width="1.5"/>
                                            </svg>
                                        </div>
                                        <div class="swiper-button-prev-1">
                                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect width="30" height="30" rx="6" fill="white"/>
                                                <path d="M12 9L18 15L12 21" stroke="#555555" stroke-width="1.5"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="second-row row">
                    </div>
                </div>
                <div class="col-md-4 sidebar">
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12 sidebar-inner">
                            <div class="aqparat">
                                <h4 class="aqparat-title">
                                    {{__('main.MainNews')}}
                                </h4>
                                <div class="aqparat-info">
                                    @foreach($mainnews as $mn_news)
                                    <div class="aqparat-info-block">
                                        @foreach($mn_news->get_categories as $category)
                                        <div  class="aqparat-info-text">
                                            <a href="{{route('categories_all',$category->id)}}" class="info-text-category">
                                                @if($category->id==1){{__('main.Culture')}}@endif
                                                @if($category->id==2){{__('main.Politics')}}@endif
                                                @if($category->id==3){{__('main.Sport')}}@endif
                                                @if($category->id==4){{__('main.Society')}}@endif
                                                @if($category->id==5){{__('main.World')}}@endif
                                                @if($category->id==6){{__('main.Economic')}}@endif
                                                @if($category->id==7){{__('main.Show Business')}}@endif
                                                @if($category->id==8){{__('main.Occasion')}}@endif
                                                @if($category->id==9){{__('main.Weather')}}@endif
                                                @if($category->id==10){{__('main.Crime')}}@endif
                                                @if($category->id==11){{__('main.Literature')}}@endif
                                                @if($category->id==13){{__('main.Court')}}@endif
                                                @if($category->id==15){{__('main.Ecology')}}@endif
                                            </a>
                                              <h3>
                                                  <a href="{{route('each_news',urlencode(str_slug($mn_news->url)))}}">
                                                      @if($currentlocale=='kz'){!!$mn_news->title!!}@endif
                                                      @if($currentlocale=='ru'){!!$mn_news->title_rus!!}@endif
                                                      @if($currentlocale=='en'){!!$mn_news->title_eng!!}@endif
                                                  </a>
                                              </h3>
                                            <span><img src="/img/icon/time.svg" alt="">{{$mn_news->date_of_creation}}</span>
                                        </div>
                                        @endforeach
                                        <div class="aqparat-info-img">
                                            @if(isset($mn_news->image))
                                            <a href="{{route('each_news',urlencode(str_slug($mn_news->url)))}}"><img style="width: 200px" src="{{urldecode(url('storage',$mn_news->image))}}" alt=""></a>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-banner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wrapper-banner">
                                <a href="{!! $banner->eventdescription  !!}" class="inner-banner" style="width: 100%">
                                    <img src="{{urldecode(url('storage', $banner->image))}}" alt="banner"  >
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="secondary-content">
        <div class="container">
            <div class="secondary-content-title">
                <p>{{__('main.Allnews')}}</p>
            </div>
            <div class="secondary-wrapper">
                <div class="secondary-block">
                    <div class="first-row row">
                        <div class="col-md-12 secondary-wrapper">
                            <div class="all-news">
                                @foreach($main as $news)
                                    @if($news->first())
                                    <div class="all-big-news">
                                        <a href="{{route('each_news',urlencode(str_slug($news->first()->url)))}}"><img src="{{urldecode(url('storage',$news->first()->image))}}" alt=""></a>
                                        <div  class="big-news-info">
                                            @foreach($news->first()->get_categories as $category)
                                            <a href="{{route('categories_all',$category->id)}}">
                                                @if($category->id==1){{__('main.Culture')}}@endif
                                                @if($category->id==2){{__('main.Politics')}}@endif
                                                @if($category->id==3){{__('main.Sport')}}@endif
                                                @if($category->id==4){{__('main.Society')}}@endif
                                                @if($category->id==5){{__('main.World')}}@endif
                                                @if($category->id==6){{__('main.Economic')}}@endif
                                                @if($category->id==7){{__('main.Show Business')}}@endif
                                                @if($category->id==8){{__('main.Occasion')}}@endif
                                                @if($category->id==9){{__('main.Weather')}}@endif
                                                @if($category->id==10){{__('main.Crime')}}@endif
                                                @if($category->id==11){{__('main.Literature')}}@endif
                                            </a>
                                            @endforeach
                                           <h4>
                                               <a href="{{route('each_news',urlencode(str_slug($news->first()->url)))}}">
                                                   @if($currentlocale=='kz'){!!$news->first()->title!!}@endif
                                                   @if($currentlocale=='ru'){!!$news->first()->title_rus!!}@endif
                                                   @if($currentlocale=='en'){!!$news->first()->title_eng!!}@endif
                                               </a>
                                           </h4>
                                            <div class="autor">
                                                <span>{{$news->first()->date_of_creation}}, {{$news->first()->get_author->name}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="all-extra-news">
                                        @foreach($news->skip(1) as $extra)
                                            <div class="extra-news-item">
                                                @if(isset($extra->image))
                                                <a href="{{route('each_news',urlencode(str_slug($extra->url)))}}"><img src="{{urldecode(url('storage', $extra->image))}}" alt=""></a>
                                                @endif
                                                <div  class="extra-news-info">
                                                    @foreach($extra->get_categories as $category)
                                                    <a href="{{route('categories_all',$category->id)}}" class="info-text-category" href="#">
                                                        @if($category->id==1){{__('main.Culture')}}@endif
                                                        @if($category->id==2){{__('main.Politics')}}@endif
                                                        @if($category->id==3){{__('main.Sport')}}@endif
                                                        @if($category->id==4){{__('main.Society')}}@endif
                                                        @if($category->id==5){{__('main.World')}}@endif
                                                        @if($category->id==6){{__('main.Economic')}}@endif
                                                        @if($category->id==7){{__('main.Show Business')}}@endif
                                                        @if($category->id==8){{__('main.Occasion')}}@endif
                                                        @if($category->id==9){{__('main.Weather')}}@endif
                                                        @if($category->id==10){{__('main.Crime')}}@endif
                                                        @if($category->id==11){{__('main.Literature')}}@endif
                                                     </a>
                                                    @endforeach
                                                    <h3>
                                                        <a href="{{route('each_news',urlencode(str_slug($extra->url)))}}">
                                                            @if($currentlocale=='kz'){!!$extra->title!!}@endif
                                                            @if($currentlocale=='ru'){!!$extra->title_rus!!}@endif
                                                            @if($currentlocale=='en'){!!$extra->title_eng!!}@endif
                                                        </a>
                                                    </h3>
                                                    <div class="autor">
                                                        <span><img src="/img/icon/time.svg" alt="">{{$extra->date_of_creation}}, {{$extra->get_author->name}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="secondary-block">
                    <div class="right-news">
                        @foreach($extranews as $news)
                        <div class="right-news-item">
                            <div class="right-news-img">

                                @if(isset($news->image))
                                    <a href="{{route('each_news', urlencode(str_slug($news->url)))}}"><img src="{{urldecode(url('storage',$news->image))}}" alt=""></a>
                                @endif
                                @if(isset($news->video))
                                    <iframe id="inlineFrameExample"
                                            title="Inline Frame Example"
                                            width="270"
                                            height="155"
                                            src="{!! $news->video !!}">
                                    </iframe>
                                @endif
                            </div>

                            <div class="right-news-info">
                                @foreach($news->get_categories as $category)
                                <a href="{{route('categories_all',$category->id)}}" class="info-text-category">
                                    @if($category->id==1){{__('main.Culture')}}@endif
                                    @if($category->id==2){{__('main.Politics')}}@endif
                                    @if($category->id==3){{__('main.Sport')}}@endif
                                    @if($category->id==4){{__('main.Society')}}@endif
                                    @if($category->id==5){{__('main.World')}}@endif
                                    @if($category->id==6){{__('main.Economic')}}@endif
                                    @if($category->id==7){{__('main.Show Business')}}@endif
                                    @if($category->id==8){{__('main.Occasion')}}@endif
                                    @if($category->id==9){{__('main.Weather')}}@endif
                                    @if($category->id==10){{__('main.Crime')}}@endif
                                    @if($category->id==11){{__('main.Literature')}}@endif
                                </a>
                                @endforeach
                                <h3>
                                    <a href="{{route('each_news',urlencode(str_slug($news->url)))}}">
                                        @if($currentlocale=='kz'){!!$news->title!!}@endif
                                        @if($currentlocale=='ru'){!!$news->title_rus!!}@endif
                                        @if($currentlocale=='en'){!!$news->title_eng!!}@endif
                                    </a>
                                </h3>
                                <div class="autor">
                                    <span><img src="/img/icon/time.svg" alt="">{{$news->date_of_creation}}, {{$news->get_author->name}}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
{{--<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>--}}
{{--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>--}}
{{--<script src="/assets/js/main.js"></script>--}}


