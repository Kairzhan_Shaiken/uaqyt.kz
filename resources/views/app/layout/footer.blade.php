<footer>
    <div class="container">
        <div class="footer-top">
            <div class="footer-logo">
                <img src="/img/uaqyt4.png" alt="" style="width: 70px; height: 70px;">
                <a href="{{route('mainpage')}}">Уaqyt.kz</a>
            </div>
            <nav>
                <ul class="footer-nav">
                    <li>
                        <a href="#"><img src="/public/img/icon/Group.svg" alt=""></a>
                    </li>
                    <li>
                        <a href="#"><img src="/public/img/icon/Group-1.svg" alt=""></a>
                    </li>
                    <li>
                        <a href="#"><img src="/public/img/icon/Group-2.svg" alt=""></a>
                    </li>
                    <li>
                        <a href="#"><img src="/public/img/icon/Group-3.svg" alt=""></a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="footer-down">
            <nav>
                <ul class="footer-menu">
                    @foreach($categories as $category)
                    <li>
                        <a href="{{route('categories_all',$category->id)}}" >
                        @if($category->id==1){{__('main.Culture')}}@endif
                        @if($category->id==2){{__('main.Politics')}}@endif
                        @if($category->id==3){{__('main.Sport')}}@endif
                        @if($category->id==4){{__('main.Society')}}@endif
                        @if($category->id==5){{__('main.World')}}@endif
                        @if($category->id==6){{__('main.Economic')}}@endif
                        @if($category->id==7){{__('main.Show Business')}}@endif
                    </li>
                    @endforeach
                    <li>
                        <a href="{{route('contacts')}}">{{__('main.Contacts')}}</a>
                    </li>
                </ul>
            </nav>
            <div class="privacy">
                <a href="{{route('rules')}}">{{__('main.Rules')}}</a>
            </div>
        </div>
    </div>
</footer>

