<header>
    <div class="container">
        <div class="header-up">
            <div class="header-data" style="display: flex; flex-direction: column; align-items: center">
                <img src="/img/uaqyt1.png" style="width: 120px; margin-bottom: 5px">
                <div style="display: flex; flex-wrap: wrap; gap: 5px">
                    <p style="font-size: smaller">{{$today->day}}</p>

                    @if($today->monthName=='январь')   <p style="font-size: smaller"> {{__('main.January')}}   </p> @endif
                    @if($today->monthName=='февраль')  <p style="font-size: smaller"> {{__('main.February')}}  </p> @endif
                    @if($today->monthName=='март')     <p style="font-size: smaller"> {{__('main.March')}}     </p> @endif
                    @if($today->monthName=='апрель')   <p style="font-size: smaller"> {{__('main.April')}}     </p> @endif
                    @if($today->monthName=='май')      <p style="font-size: smaller"> {{__('main.May')}}       </p> @endif
                    @if($today->monthName=='июнь')     <p style="font-size: smaller"> {{__('main.June')}}      </p> @endif
                    @if($today->monthName=='июль')     <p style="font-size: smaller"> {{__('main.July')}}      </p> @endif
                    @if($today->monthName=='август')   <p style="font-size: smaller"> {{__('main.August')}}    </p> @endif
                    @if($today->monthName=='сентябрь') <p style="font-size: smaller"> {{__('main.September')}} </p> @endif
                    @if($today->monthName=='октябрь')  <p style="font-size: smaller"> {{__('main.October')}}   </p> @endif
                    @if($today->monthName=='ноябрь')   <p style="font-size: smaller"> {{__('main.November')}}  </p> @endif
                    @if($today->monthName=='декабрь')  <p style="font-size: smaller"> {{__('main.December')}}  </p> @endif

                    @if($today->dayName=='понедельник') <p style="font-size: smaller"> {{__('main.Monday')}}     </p>@endif
                    @if($today->dayName=='вторник')     <p style="font-size: smaller"> {{__('main.Tuesday')}}    </p>@endif
                    @if($today->dayName=='среда')       <p style="font-size: smaller"> {{__('main.Wednesday')}}  </p>@endif
                    @if($today->dayName=='четверг')     <p style="font-size: smaller"> {{__('main.Thursday')}}   </p>@endif
                    @if($today->dayName=='пятница')     <p style="font-size: smaller"> {{__('main.Friday')}}     </p>@endif
                    @if($today->dayName=='суббота')     <p style="font-size: smaller"> {{__('main.Saturday')}}   </p>@endif
                    @if($today->dayName=='воскресенье') <p style="font-size: smaller"> {{__('main.Sunday')}}     </p>@endif

                    <p style="font-size: smaller"> {{$today->year}} </p>
                </div>
            </div>
            <a href="{{route('mainpage')}}" class="header-logo">
                <img style="width: 75px; height: 75px" src="/img/uaqyt4.png" alt="">
            </a>
            <div class="header-search">
                <form class="header-form" action="{{route('search')}}" method="post">
                    @csrf
                    @method('post')
                    <input type="search"  name="keyword" placeholder="Поиск" aria-label="Search" aria-describedby="search-addon" />
                    <i class="fas fa-search"> <button type="submit"><img src="/img/icon/search.svg" alt=""></button></i>
                </form>
                <div class="header-lang">
                    <a href="{{route('locale','kz')}}">Kz</a>
                    <a href="{{route('locale','ru')}}">Ru</a>
                    <a href="{{route('locale','en')}}">En</a>
                </div>
            </div>
        </div>
        <div class="header-up-mobile">
            <a href="{{route('mainpage')}}" class="header-logo">
                <img src="/img/uaqyt4.png" alt="">
            </a>
            <div class="header-data">
                <p style="font-size: smaller">{{$today->dayName}}, {{$today->day}} {{$today->monthName}},{{$today->year}} </p>
            </div>
            <div class="header-search">

                <form style="margin: 0" action="{{route('search')}}" method="post">
                    @csrf
                    @method('post')
                    <div class="input-group rounded">
                        <input type="search" class="form-control rounded" name="keyword" placeholder="Поиск" aria-label="Search" aria-describedby="search-addon" />
                        <span class="input-group-text border-0" id="search-addon">
                        <i class="fas fa-search"> <button type="submit"><img src="/img/icon/search.svg" alt=""></button></i>
                        </span>
                    </div>
                </form>

                <div class="header-lang">
                    <a href="{{route('locale','kz')}}">Каз</a>
                    <a href="{{route('locale','ru')}}">Рус</a>
                    <a href="{{route('locale','en')}}">Eng</a>
                </div>
            </div>
        </div>
        <div class="header-down">
            <div class="header__swiper">
                <a class="header-big-news" href="{{route('mainpage')}}">{{__('main.MainNews')}}</a>
                <div class="swiper headerSwiper">
                    <div class="swiper-wrapper">
                        @foreach($categories as $cat)
                            <nav class="swiper-slide">
                                <ul class="header-nav">
                                    <li>
                                        <a href="{{route('categories_all',$cat->id)}}" >
                                            @if($cat->id==1){{__('main.Culture')}}@endif
                                            @if($cat->id==2){{__('main.Politics')}}@endif
                                            @if($cat->id==3){{__('main.Sport')}}@endif
                                            @if($cat->id==4){{__('main.Society')}}@endif
                                            @if($cat->id==5){{__('main.World')}}@endif
                                            @if($cat->id==6){{__('main.Economic')}}@endif
                                            @if($cat->id==7){{__('main.Show Business')}}@endif
                                            @if($cat->id==8){{__('main.Occasion')}}@endif
                                            @if($cat->id==9){{__('main.Weather')}}@endif
                                            @if($cat->id==10){{__('main.Crime')}}@endif
                                            @if($cat->id==11){{__('main.Literature')}}@endif
                                            @if($cat->id==13){{__('main.Court')}}@endif
                                            @if($cat->id==15){{__('main.Ecology')}}@endif
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        @endforeach
                    </div>
                </div>
                <!-- <div class="headerSwiper-control">
                    <div class="swiper-button-next-2">
                        <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 1L2 7L8 13" stroke="#222222" stroke-width="1.5"/>
                        </svg>
                    </div>
                    <div class="swiper-button-prev-2">
                        <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1L7 7L1 13" stroke="#222222" stroke-width="1.5"/>
                        </svg>
                    </div>
                </div> -->
            </div>
        </div>
    </div>

</header>

