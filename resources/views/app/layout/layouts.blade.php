<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Уақыт.kz</title>
    <link rel="shortcut icon" type="image/x-icon" href="/img/yaqyt.png" />
    {{--    <meta name="description" content="«Еңбек – адамның екінші анасы» дейміз. Тәрбиелік мәні терең бұл тәмсілдің мағынасын «Адамды адам еткен – еңбек» деген тағы бір дана сөз аша түседі. Яғни еңбек адамды өсіреді, өмірден өз орныңды тауып, тұлға болып қалыптасуыңа бастайды, қатарға қосады. Өкі&shy;ніштісі, қазіргі кезде осын">--}}
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="/style/style.css">
    @yield('meta')
</head>
<body>
@include('app.layout.header')
<main>
    <div class="main">
        @yield('content')
    </div>
</main>
@include('app.layout.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<script src="/assets/js/main.js"></script>
</body>
