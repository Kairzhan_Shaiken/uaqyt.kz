{{--<?php $get_lang = \App\Services\LangDb::getInstance(); $get_lang->get() ?>--}}
{{--@foreach($get_lang->langs as $lang)--}}
{{--    <div class="mb-4">--}}
{{--        <label class="required form-label">Текст мәтіні</label>--}}
{{--        <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">--}}
{{--            <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>--}}
{{--        </svg>--}}
{{--        <textarea--}}
{{--            class="form-control form-control-solid"--}}
{{--            name="{{ $name.'[lang]['.$lang->code.']' }}"--}}
{{--            id="{{ $name }}"--}}
{{--            value="{{ old($name, (isset($item)) ? isset($item->{$name}) ? $item->{$item}->lang->{$lang->code} : null : null) }}"--}}
{{--            @if (isset($placeholder)) placeholder="{{ $placeholder }}" @endif--}}
{{--            @if (isset($disabled) && $disabled) disabled @endif--}}
{{--            @if($get_lang->default['id'] == $lang->id) {{ ($lang->code == $get_lang->default['code']) ? (isset($required) && $required) ? 'required' : '' : '' }} @endif--}}
{{--            autocomplete="off">--}}
{{--        </textarea>--}}
{{--    </div>--}}
{{--@endforeach--}}

{{--<div class="tab-content">--}}
{{--    Kazakh--}}
{{--    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">--}}
{{--        <div class="tab-content">--}}
{{--            <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">--}}
{{--                <div class="mb-4">--}}
{{--                    <label class="required form-label">Тақырып--}}
{{--                        <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">--}}
{{--                            <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>--}}
{{--                        </svg>--}}
{{--                    </label>--}}
{{--                    <input type="text" class="form-control form-control-solid" name="title" placeholder="Введите заголовок" required>--}}
{{--                </div>--}}
{{--                <div class="mb-4">--}}
{{--                    <label class="required form-label">Текст мәтіні</label>--}}
{{--                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">--}}
{{--                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>--}}
{{--                    </svg>--}}
{{--                    <textarea name="content" required></textarea>--}}
{{--                    @include('admin._includes.lang.text_area', [--}}
{{--                             'name' => 'title',--}}
{{--                             'placeholder' => 'Текст мәтіні',--}}
{{--                             'item' => isset($item) ? $item : '',--}}
{{--                             'required' => true--}}
{{--                             ])--}}
{{--                    <script>--}}
{{--                        CKEDITOR.replace( 'content' );--}}
{{--                        CKEDITOR.config.allowedContent = true;--}}
{{--                        CKEDITOR.config.removeFormatAttributes = '';--}}
{{--                        CKEDITOR.config.filebrowserImageBrowseUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";--}}
{{--                        CKEDITOR.config.filebrowserUploadUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";--}}
{{--                        CKEDITOR.config.filebrowserUploadMethod= 'form'--}}

{{--                    </script>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    Russian--}}
{{--    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">--}}
{{--        <div class="tab-content">--}}
{{--            <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">--}}
{{--                <div class="mb-4">--}}
{{--                    <label class="required form-label">Заголовок--}}
{{--                        <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">--}}
{{--                            <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>--}}
{{--                        </svg>--}}
{{--                    </label>--}}
{{--                    <input type="text" class="form-control form-control-solid" name="title" placeholder="Введите заголовок" required>--}}
{{--                    @include('admin._includes.lang.text_area', [--}}
{{--                              'name' => 'title',--}}
{{--                              'label' => 'Заголовок',--}}
{{--                              'item' => isset($item) ? $item : '',--}}
{{--                              'required' => true--}}
{{--                              ])--}}
{{--                </div>--}}
{{--                <div class="mb-4">--}}
{{--                    <label class="required form-label">Содержание</label>--}}
{{--                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">--}}
{{--                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>--}}
{{--                    </svg>--}}
{{--                    <textarea name="content1" required></textarea>--}}
{{--                    @include('admin._includes.lang.text_area', [--}}
{{--                             'name' => 'content',--}}
{{--                             'label' => 'Содержание',--}}
{{--                             'item' => isset($item) ? $item : '',--}}
{{--                             'required' => true--}}
{{--                             ])--}}
{{--                    <script>--}}
{{--                        CKEDITOR.replace( 'content1' );--}}
{{--                        CKEDITOR.config.allowedContent = true;--}}
{{--                        CKEDITOR.config.removeFormatAttributes = '';--}}
{{--                        CKEDITOR.config.filebrowserImageBrowseUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";--}}
{{--                        CKEDITOR.config.filebrowserUploadUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";--}}
{{--                        CKEDITOR.config.filebrowserUploadMethod= 'form'--}}

{{--                    </script>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    English--}}
{{--    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">--}}
{{--        <div class="tab-content">--}}
{{--            <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">--}}
{{--                <div class="mb-4">--}}
{{--                    <label class="required form-label">Title--}}
{{--                        <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">--}}
{{--                            <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>--}}
{{--                        </svg>--}}
{{--                    </label>--}}
{{--                    <input type="text" class="form-control form-control-solid" name="title" placeholder="Введите заголовок" required>--}}
{{--                    @include('admin._includes.lang.text_area', [--}}
{{--                           'name' => 'title',--}}
{{--                           'label' => 'Title',--}}
{{--                           'item' => isset($item) ? $item : '',--}}
{{--                           'required' => true--}}
{{--                           ])--}}
{{--                </div>--}}
{{--                @include('admin._includes.lang.text_area', [--}}
{{--                           'name' => 'content',--}}
{{--                           'label' => 'Content',--}}
{{--                           'item' => isset($item) ? $item : '',--}}
{{--                           'required' => true--}}
{{--                           ])--}}
{{--                <div class="mb-4">--}}
{{--                    <label class="required form-label">Content</label>--}}
{{--                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">--}}
{{--                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>--}}
{{--                    </svg>--}}
{{--                    <textarea name="content2" required id="eng"></textarea>--}}
{{--                    <script>--}}
{{--                        CKEDITOR.replace( 'content2' );--}}
{{--                        CKEDITOR.config.allowedContent = true;--}}
{{--                        CKEDITOR.config.removeFormatAttributes = '';--}}
{{--                        CKEDITOR.config.filebrowserImageBrowseUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";--}}
{{--                        CKEDITOR.config.filebrowserUploadUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";--}}
{{--                        CKEDITOR.config.filebrowserUploadMethod= 'form'--}}
{{--                    </script>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
