<?php $get_lang = \App\Services\LangDb::getInstance(); $get_lang->get() ?>
<ul class="nav nav-tabs" id="langTab" role="tablist">

    @foreach($get_lang->langs as $lang)
        <li class="nav-item" role="presentation">
            <button class="nav-link {{ $get_lang->default['code'] == $lang->code ? 'active' : '' }}" id="langtab_{{ $lang->code }}" data-bs-toggle="tab" data-bs-target="#{{ $lang->code }}" type="button" role="tab" aria-controls="{{ $lang->code }}" aria-selected="true">{{ $lang->name }}</button>
        </li>
    @endforeach
</ul>
