<?php $get_lang = \App\Services\LangDb::getInstance(); $get_lang->get() ?>
@foreach($get_lang->langs as $lang)
    <div class="tab-content">
        <div class="tab-pane fade show {{ $get_lang->default['code'] == $lang->code ? 'active' : '' }}" id="{{ $lang->code }}" role="tabpanel" aria-labelledby="langtab_{{ $lang->code }}">
            <div class="tab-content">
                <div class="tab-pane fade show {{ $get_lang->default['code'] == $lang->code ? 'active' : '' }}" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                    <div class="mb-4">
                        <label class="required form-label">{{ $label }}
                            <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                            </svg>
                        </label>
                        <input
                            type="text"
                            class="form-control form-control-solid"
                            name="{{ $name.'[lang]['.$lang->code.']' }}"
                            id="{{ $name }}"
                            value="{{ old($name, (isset($item)) ? isset($item->{$name}) ? $item->{$item}->lang->{$lang->code} : null : null) }}"
                            @if (isset($placeholder)) placeholder="{{ $placeholder }}" @endif
                            @if (isset($disabled) && $disabled) disabled @endif
                            @if($get_lang->default['id'] == $lang->id) {{ ($lang->code == $get_lang->default['code']) ? (isset($required) && $required) ? 'required' : '' : '' }} @endif
                            autocomplete="off"
                        >
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
