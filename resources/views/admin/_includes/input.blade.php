<div class="mb-4">
    <label class="form-label">{{ $placeholder }}</label>
    <input
        type="text"
        class="form-control form-control-solid"
        name="{{ $name }}"
        @if (isset($placeholder)) placeholder="{{ $placeholder }}" @endif
        @if (isset($disabled) && $disabled) disabled @endif
        @if (isset($required) && $required) required @endif
        value=""
    ><!---->
</div>
