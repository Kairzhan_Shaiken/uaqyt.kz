@extends('admin.layout.admin')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="header">
                <div class="header-body">
                    Реклама <a href="" class="btn btn-primary btn-sm" style="margin-left: 900px"> Назад </a>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                    <div class="card" data-list>
                        <form action="{{route('update-advertisment',$certain_advertisment->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <div class="row" style="display: flex; justify-content: center;">
                                <div class="card col-12 col-lg-7 m-3">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="kaz" role="tabpanel">
{{--                                                <div class="mb-4">--}}
{{--                                                    <label class="required form-label">Заголовок</label>--}}
{{--                                                     <p style="color: red"> * поле обязательно для заполнения</p>--}}
{{--                                                    <input type="text" value="{{$certain_advertisment->title}}" class="form-control form-control-solid" name="title" placeholder="Введите заголовок" required>--}}
{{--                                                </div>--}}
                                                <div class="mb-4">
                                                    <label class="required form-label">Ссылка</label>
                                                    <p style="color: red"> * поле обязательно для заполнения</p>
                                                    <textarea name="eventdescription" required>{{$certain_advertisment->eventdescription}}</textarea>
                                                    <script>
                                                        CKEDITOR.replace( 'eventdescription' );
                                                        CKEDITOR.config.allowedContent = true;
                                                        CKEDITOR.config.removeFormatAttributes = '';
                                                        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <button class="btn btn-primary"  type="submit" onclick="myFunction()"> Сохранить</button>
                                    </div>
                                </div>
                                <div class="card col-12 col-lg-4 m-3 h-50">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <label class="required form-label">Изображение</label>
                                            <p style="color: red"> * выберите изображение</p>
                                            <input accept="image/*" type="file" class="form-control form-control-solid" name="image" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="card-footer d-flex justify-content-between">
                            {{--                            {{ $row->withQueryString()->links() }}--}}
                            {{--                            <x-admin.select-action/>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
