@extends('admin.layout.admin')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="header">
                <div class="header-body">
                    Изменить Пользователя <a href="" class="btn btn-primary btn-sm" style="margin-left: 900px"> Назад </a>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                    <div class="card" data-list>
                        <form action="{{route('update-user',$certain_user)}}" method="post">
                            @method('POST')
                            @csrf
                            <div class="row">
                                <div class="card col-12 col-lg-7 m-3">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="kaz" role="tabpanel">
                                                <div class="mb-4">
                                                    <label class="required form-label"> Изменить имя</label>
                                                    <input value="{{$certain_user->name}}" type="text" class="form-control form-control-solid" name="name">
                                                </div>

                                                <div class="mb-4">
                                                    <label class="required form-label">Изменить Email</label>
                                                    <input value="{{$certain_user->email}}" type="text" class="form-control form-control-solid" name="email">
                                                </div>

                                                <div class="mb-4">
                                                    <label class="required form-label"> Новый Пароль</label>
                                                    <input  type="text" class="form-control form-control-solid" name="password">
                                                </div>

                                            </div>
                                        </div>
                                        <hr>
                                        <button class="btn btn-primary" type="submit"> Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="card-footer d-flex justify-content-between">
                            {{--                            {{ $row->withQueryString()->links() }}--}}
                            {{--                            <x-admin.select-action/>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

