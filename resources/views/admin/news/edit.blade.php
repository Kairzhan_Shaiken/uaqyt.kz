@extends('admin.layout.admin')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="header">
                <div class="header-body">
                    Изменить Новость
                    <a href="{{route("allnews")}}" class="btn btn-primary btn-sm" style="margin-left: 900px"> Назад </a>
                </div>
            </div>
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Қазақша</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Руский</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">English</button>
                </li>
            </ul>
            <form action="{{route('update-news',$item->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="card" style="display: flex; flex-direction: inherit">
                    <div class="right-block">
                        <div class="tab-content" id="pills-tabContent">
                            {{--kaz--}}
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                                        <div>
                                            <div  style="display: flex;">
                                                <div class="card m-3">
                                                    <div class="card-body">
                                                        <div class="tab-content">
                                                            <div class="tab-pane fade show active" id="kaz" role="tabpanel">
                                                                <div class="mb-4">
                                                                    <label class="required form-label">Тақырып </label>
                                                                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                                                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                                                    </svg>
                                                                    <input value="{{$item->title}}" type="text" class="form-control form-control-solid" name="title" placeholder="Введите заголовок" required>
                                                                </div>

                                                                <div class="mb-4">
                                                                    <label class="required form-label">Мақала мәтіні</label>
                                                                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                                                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                                                    </svg>
                                                                    <textarea name="content" required>{{$item->content}}</textarea>
                                                                    <script>
                                                                        CKEDITOR.replace( 'content' );
                                                                        CKEDITOR.config.allowedContent = true;
                                                                        CKEDITOR.config.removeFormatAttributes = '';
                                                                        CKEDITOR.config.filebrowserUploadUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";
                                                                        CKEDITOR.config.filebrowserUploadMethod= 'form'
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--rus--}}
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                                        <div>
                                            <div  style="display: flex;">
                                                <div class="card m-3">
                                                    <div class="card-body">
                                                        <div class="tab-content">
                                                            <div class="tab-pane fade show active" id="kaz" role="tabpanel">
                                                                <div class="mb-4">
                                                                    <label class="required form-label">Заголовок</label>
                                                                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                                                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                                                    </svg>
                                                                    <input value="{{$item->title_rus}}" type="text" class="form-control form-control-solid" name="title_rus" placeholder="Введите заголовок" required>
                                                                </div>

                                                                <div class="mb-4">
                                                                    <label class="required form-label">Содержание</label>
                                                                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                                                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                                                    </svg>
                                                                    <textarea name="content_rus" required>{{$item->content_rus}}</textarea>
                                                                    <script>
                                                                        CKEDITOR.replace( 'content_rus' );
                                                                        CKEDITOR.config.allowedContent = true;
                                                                        CKEDITOR.config.removeFormatAttributes = '';
                                                                        CKEDITOR.config.filebrowserUploadUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";
                                                                        CKEDITOR.config.filebrowserUploadMethod= 'form'
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--eng--}}
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                                        <div>
                                            <div  style="display: flex;">
                                                <div class="card m-3">
                                                    <div class="card-body">
                                                        <div class="tab-content">
                                                            <div class="tab-pane fade show active" id="kaz" role="tabpanel">
                                                                <div class="mb-4">
                                                                    <label class="required form-label">Title</label>
                                                                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                                                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                                                    </svg>
                                                                    <input value="{{$item->title_eng}}" type="text" class="form-control form-control-solid" name="title_eng" placeholder="Введите заголовок" required>
                                                                </div>

                                                                <div class="mb-4">
                                                                    <label class="required form-label">Content</label>
                                                                    <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                                                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                                                    </svg>
                                                                    <textarea name="content_eng" required>{{$item->content_eng}}</textarea>
                                                                    <script>
                                                                        CKEDITOR.replace( 'content_eng' );
                                                                        CKEDITOR.config.allowedContent = true;
                                                                        CKEDITOR.config.removeFormatAttributes = '';
                                                                        CKEDITOR.config.filebrowserUploadUrl = "{{ route('upload-image', ['_token' => csrf_token()]) }}";
                                                                        CKEDITOR.config.filebrowserUploadMethod= 'form'
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card m-3">
                            {{--SEO--}}
                            <div class="card-body">
                                <div class="mb-4">
                                    <label class="form-label">SEO заголовок</label>
                                    <input value="{{$item->SEOtitles}}" type="text" class="form-control form-control-solid" name="seotitles"><!---->
                                </div>
                                <div class="mb-4">
                                    <label class="form-label">SEO описание</label>
                                    <input value="{{$item->SEOdescriptions}}" type="text" class="form-control form-control-solid" name="seodescriptions"><!---->
                                </div>
                                <div class="mb-4">
                                    <label class="form-label">SEO ключевые слова</label>
                                    <input value="{{$item->SEOkeywords}}" type="text" class="form-control form-control-solid" name="seokeywords">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card m-3" style="width: 32%">  {{--вторая часть--}}
                        <div class="card-body">
                            <div class="mb-4">
                                <label class="required form-label">Автор</label>
                                <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                    <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                </svg>
                                <select class="form-control form-control-solid" aria-label="Default select example" name="author_id" style="width: 150px">
                                    @foreach($users as $user)
                                        <option selected value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-4">
                                <label class="required form-label">Изображение</label>
                                                                            <p style="color: red"> * выберите изображение</p>
                                <input accept="image/video/*" type="file" class="form-control form-control-solid" name="image" >
                            </div>
                            <div class="mb-4">
                                <label class="form-label">URL видео (ссылка)</label>
                                <input type="text" class="form-control form-control-solid" name="video">
                            </div>
                            <div class="mb-4">
                                <label class="form-label">Описание изображения</label>
                                <input type="text" class="form-control form-control-solid" name="image_description">
                            </div>
                            <div class="mb-4">
                                <label class="required form-label">Выберите Категорию</label>
                                <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                    <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                </svg>
                                <br>
                                @foreach($categories as $category)
                                    <input value="{{$category->id}}" class="form-check-input"  type="checkbox"  id="flexCheckDefault"  name="сategories[]">
                                    <label class="form-check-label" for="flexCheckDefault">{{$category->name}}</label>    <br>
                                @endforeach
                            </div>
                            <div class="mb-4">
                                <label class="required form-label">Выберите Рубрику</label>
                                <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                    <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                </svg>
                                <br>
                                @foreach($rubrics as $rubric)
                                    <input value="{{$rubric->id}}" class="form-check-input"  type="checkbox"  id="flexCheckDefault"  name="rubrics[]">
                                    <label class="form-check-label" for="flexCheckDefault">{{$rubric->name}}</label>    <br>
                                @endforeach
                            </div>
                            <div class="mb-4">
                                <label class="required form-label">Дата</label>
                                <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                    <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                </svg>
                                <input class="form-control form-control-solid" type="datetime-local" name="date_of_creation" required><!---->
                            </div>
                            <div class="mb-4">
                                <label >Отображать</label>
                                <svg style="color: red" xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                    <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                </svg>
                                <select class="form-control form-control-solid" name="visibility" required>
                                    <option value="1">Да</option>
                                    <option value="0">Нет</option>
                                </select><!---->
                            </div>
                            <button class="btn btn-primary"  type="submit" onclick="myFunction()"> Сохранить</button>
                        </div> {{--вторая часть--}}
                    </div>
                </div>
            </form>


        </div>
@endsection
