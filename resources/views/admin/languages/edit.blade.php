@extends('admin.layout.admin')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="header">
                <div class="header-body">
                    Общее Положение        <button type="submit" class="btn btn-primary  btn-sm" style="margin-left: 900px">Назад </button>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                    <form action="{{route('update-language', $certain_language->id)}}" method="post">
                        @csrf
                        @method('POST')
                        <div class="row">
                            <div class="card col-12 col-lg-7 m-3">
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kaz" role="tabpanel">
                                            <div class="mb-4">
                                                <label class="required form-label">Имя</label>
                                                <input  value="{{$certain_language->name}}" type="text" class="form-control form-control-solid" name="name" placeholder="Введите заголовок" required>
                                            </div>

                                            <div class="mb-4">
                                                <label class="required form-label">Код</label>
                                                <input  value="{{$certain_language->code}}" type="text" class="form-control form-control-solid" name="code" placeholder="Введите заголовок" required>
                                            </div>

                                        </div>
                                    </div>
                                    <hr>
                                    <button class="btn btn-primary"  type="submit" onclick="myFunction()"> Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

