@extends('admin.layout.admin')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="header">
                <div class="header-body">
                  Статистика просмотров
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="companiesListPane" role="tabpanel" aria-labelledby="companiesListTab">
                    <div class="card" data-list>
                        <table class="table table-striped" style="text-align: center">
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">Просмотры</th>
                                <th scope="col">Изображение</th>
                                <th scope="col">Заголовок</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allnews as $news)
                            <tr>
                                <th scope="row">{{$news->id}}</th>
                                <td>{{$news->views}}</td>
                                <td>
                                    @if(isset($news->image))
                                    <img src="{{urldecode(url('storage',$news->image))}}" style="width: 50px"></td>
                                    @endif
                                    @if(isset($news->video))
                                    <iframe id="inlineFrameExample"
                                            title="Inline Frame Example"
                                            width="60"
                                            height="60"
                                            src="{!! $news->video!!}">
                                    </iframe>
                                    @endif
                                <td>{{$news->title}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="card-footer d-flex justify-content-between">
                                {{$allnews->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
