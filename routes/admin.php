<?php


use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\Advertisment\AdvertismentController;
use App\Http\Controllers\Admin\News\NewsController;
use App\Http\Controllers\Admin\Categories\CategoriesController;
use App\Http\Controllers\Admin\Pages\PagesController;
use App\Http\Controllers\Admin\Rubric\RubricController;
use \App\Http\Controllers\Admin\Languages\LanguagesController;
use \App\Http\Controllers\Admin\Statistics\StatisticsController;

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Admin\auth\AuthController;

use \App\Http\Controllers\Admin\User\UserController;
use \App\Http\Controllers\Admin\Image\ImageController;
use \App\Http\Controllers\Admin\test\TestController;

    Route::group(['namespace'=> 'Admin'], function () {
    Route::get('admin', [ AuthController::class, 'index' ]);
    Route::post('authorisation', [ AuthController::class, 'auth' ])->name('auth');
//  Route::get('logout', [ AuthController::class, 'logout' ])->name('logout');


    Route::post('/authorisation', [AuthController::class, 'auth'])->name('auth');

    //Categories
    Route::get('/categories',  [CategoriesController::class, 'index'])->name('categories');
    Route::get('/create-categories',  [CategoriesController::class, 'create'])->name('create-categories');
    Route::post('/add_categories_to_db',  [CategoriesController::class, 'store'])->name('add_categories_to_db');
    Route::get('/edit-categories{category_id}',  [CategoriesController::class, 'show'])->name('edit-categories');
    Route::post('/update-categories{category_id}',  [CategoriesController::class, 'update'])->name('update-categories');
    Route::get('/delete-categories{category_id}',  [CategoriesController::class, 'destroy'])->name('delete-categories');

    //News
    Route::get('/allnews',  [NewsController::class, 'index'])->name('allnews');
    Route::get('/create-news',  [NewsController::class, 'create'])->name('create-news');
    Route::post('/add-news-to-db',  [NewsController::class, 'store'])->name('add_news_to_db');
    Route::get('/edit-news{news_id}',  [NewsController::class, 'show'])->name('edit-news');
    Route::get('/delete-news{news_id}',  [NewsController::class, 'destroy'])->name('delete-news');
    Route::post('/update-news{news_id}',  [NewsController::class, 'update'])->name('update-news');

    //Pages
    Route::get('/pages-list',  [PagesController::class, 'index'])->name('allpages');
    Route::get('/create-pages',  [PagesController::class, 'create'])->name('create-pages');
    Route::post('/add-pages-to-db',  [PagesController::class, 'store'])->name('add_pages_to_db');
    Route::get('/edit-pages{pages_id}',  [PagesController::class, 'show'])->name('edit-pages');
    Route::get('/delete-pages{pages_id}',  [PagesController::class, 'destroy'])->name('delete-pages');
    Route::post('/update-pages{news_id}',  [PagesController::class, 'update'])->name('update-pages');

    //Rubrics
    Route::get('/rubric-list',  [RubricController::class, 'index'])->name('rubric');
    Route::get('/create-rubric',  [RubricController::class, 'create'])->name('create-rubric');
    Route::post('/add_rubric-to-db',  [RubricController::class, 'store'])->name('add_rubric_to_db');
    Route::get('/edit-rubric{rubric_id}',  [RubricController::class, 'show'])->name('edit-rubric');
    Route::get('/delete-rubric{rubric_id}',  [RubricController::class, 'destroy'])->name('delete-rubric');
    Route::post('/update-rubric{rubric_id}',  [RubricController::class, 'update'])->name('update-rubric');

    //Advertisment
    Route::get('advertisment',[AdvertismentController::class,'index'])->name('advertisment');
    Route::get('create-advertisment',[AdvertismentController::class,'create'])->name('create-advertisment');
    Route::post('add-banner_to-db',[AdvertismentController::class,'store'])->name('add-banner_to-db');
    Route::get('/edit-advertisment{advertisment_id}',  [AdvertismentController::class, 'show'])->name('edit-advertisment');
    Route::get('/delete-advertisment{advertisment_id}',  [AdvertismentController::class, 'destroy'])->name('delete-advertisment');
    Route::post('/update-advertisment{advertisment_id}',  [AdvertismentController::class, 'update'])->name('update-advertisment');

    //User
    Route::get('users',[UserController::class,'index'])->name('users');
    Route::get('create-user',[UserController::class,'create'])->name('create-users');
    Route::post('add-user_to-db',[UserController::class,'store'])->name('add-user_to-db');
    Route::get('edit-user{user_id}',[UserController::class,'show'])->name('edit-user');
    Route::post('update-user{user_id}',  [UserController::class, 'update'])->name('update-user');
    Route::get('delete-user{user_id}',[UserController::class,'destroy'])->name('delete-user');

    //Statictics
    Route::get('statistics',               [StatisticsController::class,'index'])->name('statistics');

    //Image
    Route::post('/upload-image',           [ImageController::class, 'fileUploads'])->name('upload-image');

//    //Languages
//    Route::get('/languages',               [LanguagesController::class, 'index'])->name('languages');
//    Route::get('/create-language',         [LanguagesController::class,'create'])->name('create-language');
//    Route::post('/add-language-to-db',     [LanguagesController::class,'store'])->name('add-language-to-db');
//    Route::get('/edit-language{lang_id}',  [LanguagesController::class,'show'])->name('edit-language');
//    Route::post('update-language{lang_id}',[LanguagesController::class, 'update'])->name('update-language');
//    Route::get('/delete-language{lang_id}',[LanguagesController::class,'destroy'])->name('delete-language');

});
