<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();

            $table->string('image')->nullable();
            $table->string('title');
            $table->unsignedBigInteger('author_id')->nullable();
            $table->longText('content')->nullable();

            $table->text('SEOtitles')->nullable();
            $table->text('SEOdescriptions')->nullable();
            $table->text('SEOkeywords')->nullable();

            $table->date('date_of_creation');
            $table->string('visibility');


            $table->integer('views')->nullable();
            $table->string('type')->nullable();

            $table->timestamps();

            $table->foreign('author_id','author_user_fk')->on('users')->references('id');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
};
